const reverseWords = (string) => {
  return string.split(" ").reverse().join(" ");
};

console.log(reverseWords(" the sky is blue"));
