const leftSlide = (row) => {
  const slide = (arr) =>
    arr.sort((a, b) => {
      if (b === 0) {
        return -1;
      }
    });
  const newRow = slide(row);
  for (let i = 0; i < newRow.length; i++) {
    if (newRow === 0) continue;
    if (newRow[i] === newRow[i + 1]) {
      newRow[i] = newRow[i] + newRow[i + 1];
      newRow[i + 1] = 0;
    }
  }

  return slide(newRow);
};

console.log(leftSlide([2, 2, 2, 0]));
console.log(leftSlide([2, 2, 4, 4, 8, 8]));
console.log(leftSlide([0, 2, 0, 2, 4]));
console.log(leftSlide([0, 2, 2, 8, 8, 8]));
