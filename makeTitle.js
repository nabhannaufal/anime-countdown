const makeTitle = (str) => {
  const words = str.split(" ");
  return words
    .map((word) => {
      return word[0].toUpperCase() + word.substring(1);
    })
    .join(" ");
};

console.log(makeTitle("This is a title")); // ➞ "This Is A Title"
console.log(makeTitle("capitalize every word")); // ➞ "Capitalize Every Word"
console.log(makeTitle("I Like Pizza")); // ➞ "I Like Pizza"
console.log(makeTitle("PIZZA PIZZA PIZZA")); // ➞ "PIZZA PIZZA PIZZA"
console.log(makeTitle("I am a title")); // ➞ "I Am A Title"
console.log(makeTitle("I AM A TITLE")); // ➞ "I AM A TITLE"
console.log(makeTitle("i aM a tITLE")); // ➞ "I AM A TITLE"
console.log(makeTitle("the first letter of every word is capitalized")); // ➞ "The First Letter Of Every Word Is Capitalized"
console.log(makeTitle("I Like Pizza")); // ➞ "I Like Pizza"
console.log(makeTitle("Don't count your ChiCKens BeFore They HatCh")); // ➞ "Don't Count Your ChiCKens BeFore They HatCh"
console.log(makeTitle("All generalizations are false, including this one")); // ➞ "All Generalizations Are False, Including This One"
console.log(makeTitle("Me and my wife lived happily for twenty years and then we met.")); // ➞ "Me And My Wife Lived Happily For Twenty Years And Then We Met."
console.log(makeTitle("There are no stupid questions, just stupid people.")); // ➞ "There Are No Stupid Questions, Just Stupid People."
console.log(makeTitle("1f you c4n r34d 7h15, you r34lly n33d 2 g37 l41d")); // ➞ "1f You C4n R34d 7h15, You R34lly N33d 2 G37 L41d"
