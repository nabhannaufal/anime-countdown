import chalk from "chalk";

const formatTime = (time) => {
  return time.toString().padStart(2, "0");
};

const displayTime = (time, remainingTime) => {
  if (remainingTime < 10) {
    return chalk.red(time);
  }
  if (remainingTime < 60) {
    return chalk.yellow(time);
  }
  return chalk.green(time);
};

const countdown = (seconds = 0) => {
  console.log("===> Start Countdown <===");
  let remainingTime = seconds;

  const interval = setInterval(() => {
    const totalMinutes = Math.floor(remainingTime / 60);
    const hour = formatTime(Math.floor(totalMinutes / 60));
    const minute = formatTime(totalMinutes % 60);
    const second = formatTime(remainingTime % 60);
    const time = `${hour}:${minute}:${second}`;
    console.log(displayTime(time, remainingTime));
    --remainingTime;
    if (remainingTime < 0) {
      console.log("===> Countdown Finished <===");
      clearInterval(interval);
    }
  }, 1000);
};

countdown(80);
