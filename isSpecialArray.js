const isSpecialArray = (arr) => {
  let special = [];
  arr.forEach((item, index) => {
    if (index % 2 === 0) {
      if (item % 2 === 0) {
        special.push(1);
      } else {
        special.push(0);
      }
    } else {
      if (item % 2 === 0) {
        special.push(0);
      } else {
        special.push(1);
      }
    }
  });
  return Boolean(special.reduce((a, b) => a * b));
};

console.log(isSpecialArray([2, 7, 4, 9, 6, 1, 6, 3]));
