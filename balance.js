const balance = (str) => {
  const alphaVal = (s) => s.toLowerCase().charCodeAt(0) - 97 + 1;
  const countVal = (arr) => {
    return arr.reduce((a, b) => a + alphaVal(b), 0);
  };
  const splitStr = str.split("");
  const centerIndex = Math.floor(splitStr.length / 2);
  const left = splitStr.slice(0, centerIndex);
  const right = splitStr.slice(-centerIndex);
  const leftValue = countVal(left);
  const rightValue = countVal(right);
  return leftValue === rightValue;
};

balance("brake");
