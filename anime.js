import fetch from "node-fetch";
import moment from "moment";
import _ from "lodash";

const { orderBy } = _;
const urlAnime = "https://api.jikan.moe/v4/recommendations/anime";
const urlAnimeDetail = "https://api.jikan.moe/v4/anime";

const getDetailAnime = async (id) => {
  try {
    const response = await fetch(`${urlAnimeDetail}/${id}`);
    const responseJson = await response.json();
    return responseJson;
  } catch (error) {
    return null;
  }
};

const getData = async () => {
  try {
    console.log("===> Get Anime ID <===");
    const response = await fetch(urlAnime);
    const responseJson = await response.json();
    const data = responseJson?.data;
    const filterData = data?.slice(0, 10);
    const animeList = filterData?.map((item) => item.entry).flat();
    const animeById = animeList?.map((item) => item.mal_id);
    console.table(animeById);
    let count = 0;
    let anime = [];
    console.log("===> Get Anime Detail <===");
    const getAnimeDetail = setInterval(async () => {
      const detailAnime = await getDetailAnime(animeList[count].mal_id);
      const title = detailAnime?.data?.title;
      const episodes = detailAnime?.data?.episodes;
      const aired = detailAnime?.data?.aired;
      const rank = detailAnime?.data?.rank;
      const popularity = detailAnime?.data?.popularity;
      console.log(`=> Add detail ${title}`);
      if (title) {
        anime.push({
          Title: title,
          Episodes: episodes,
          Release: moment(aired.from).format("L"),
          Rank: rank,
          Popularity: popularity,
        });
      }

      count++;
      if (count === animeList.length) {
        const animeByRelease = orderBy(anime, (item) => new Date(item.Release), ["asc"]);
        const animeByRank = orderBy(anime, ["Rank"], ["asc"]).slice(0, 5);
        const animeByPopularity = orderBy(anime, ["Popularity"], ["desc"]).slice(0, 5);
        const animeByEpisode = orderBy(anime, ["Episodes"], ["desc"])[0];
        console.log("===> List Anime <===");
        console.table(anime);
        console.log("===> Anime by Release Date <===");
        console.table(animeByRelease);
        console.log("===> 5 Most Popular Anime <===");
        console.table(animeByPopularity);
        console.log("===> 5 High Rank Anime <===");
        console.table(animeByRank);
        console.log("===> Most Episode Anime <===");
        console.table([animeByEpisode]);
        clearInterval(getAnimeDetail);
      }
    }, 6000);
  } catch (error) {
    console.log("an error occured!");
  }
};

getData();
