const getBudgets = (arr) => {
  let budget = 0;
  arr.forEach((item) => {
    budget += item.budget;
  });
  return budget;
};

console.log(
  getBudgets([
    { name: "John", age: 21, budget: 23000 },
    { name: "Steve", age: 32, budget: 40000 },
    { name: "Martin", age: 16, budget: 2700 },
  ])
);
