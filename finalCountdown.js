const finalCountdown = (arr) => {
  let count = 0;
  const sequenceArray = [];
  const countData = arr.reverse();

  const collectSequence = (arr) => {
    const sequence = [];
    for (let i = 0; i < arr.length; i++) {
      sequence.unshift(arr[i]);
      if (arr[i] !== arr[i + 1] - 1) break;
    }
    sequenceArray.unshift(sequence);
  };

  for (let i = 0; i < countData.length; i++) {
    if (countData[i] === 1) {
      collectSequence(countData.slice(i));
      count++;
    }
  }
  return [count, sequenceArray];
};

console.log(finalCountdown([4, 4, 5, 4, 3, 2, 1, 8, 3, 2, 1]));
