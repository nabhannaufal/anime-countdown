const awardPrizes = (names) => {
  const sortNames = Object.entries(names).sort((a, b) => b[1] - a[1]);
  const newObject = sortNames.map((item, index) => {
    if (index === 0) return [item[0], "Gold"];
    if (index === 1) return [item[0], "Silver"];
    if (index === 2) return [item[0], "Bronze"];
    return [item[0], "Participation"];
  });
  const name = Object.fromEntries(newObject);
  const prizeName = Object.entries(names).map((item) => {
    return [item[0], name[item[0]]];
  });
  return Object.fromEntries(prizeName);
};

console.log(
  awardPrizes({
    "Person A": 1,
    "Person B": 2,
    "Person C": 3,
    "Person D": 4,
    "Person E": 102,
  })
);
