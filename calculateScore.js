// player A = Abigail
// player B = Benson

const calculateScore = (games) => {
  let scoreA = 0;
  let scoreB = 0;
  const suwit = (A, B) => {
    if (A === "R" && B === "R") return "Tie";
    if (A === "R" && B === "S") return "A";
    if (A === "R" && B === "P") return "B";
    if (A === "P" && B === "P") return "Tie";
    if (A === "P" && B === "R") return "A";
    if (A === "P" && B === "S") return "B";
    if (A === "S" && B === "S") return "Tie";
    if (A === "S" && B === "P") return "A";
    if (A === "S" && B === "R") return "B";
  };

  games.forEach((item) => {
    const score = suwit(item[0], item[1]);
    if (score === "A") scoreA++;
    if (score === "B") scoreB++;
  });

  if (scoreA === scoreB) return "Tie";
  if (scoreA > scoreB) return "Abigail";
  return "Benson";
};

console.log(
  calculateScore([
    ["R", "P"],
    ["R", "S"],
    ["S", "P"],
  ])
);
