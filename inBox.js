const inBox = (arr) => {
  let result = false;
  arr.slice(1, -1).forEach((item) => {
    if (/#\s*\*\s*#/.test(item)) result = true;
  });
  return result;
};

console.log(inBox(["####", "#* #", "#  #", "####"]));
