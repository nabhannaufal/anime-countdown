const removeDups = (arr) => {
  return arr.filter((item, index) => {
    return arr.indexOf(item) === index;
  });
};

console.log(removeDups([1, 0, 1, 0]));
