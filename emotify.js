const emotify = (str) => {
  const emoticon = {
    smile: ":D",
    grin: ":)",
    sad: ":(",
    mad: ":P",
  };
  const word = str.split(" ");
  return `${word[0]} ${word[1]} ${emoticon[word[2]]}`;
};

console.log(emotify("Make me smile")); // ➞ "Make me :D"
console.log(emotify("Make me grin")); // ➞ "Make me :)"
console.log(emotify("Make me sad")); // ➞ "Make me :("
console.log(emotify("Make me smile")); // ➞ "Make me :D"
console.log(emotify("Make me grin")); // ➞ "Make me :)"
console.log(emotify("Make me sad")); // ➞ "Make me :("
console.log(emotify("Make me mad")); // ➞ "Make me :P"
