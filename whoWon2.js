const whoWon = (board) => {
  const r1 = board[0];
  const r2 = board[1];
  const r3 = board[2];
  const win = [];

  if (r1[0] === r1[1] && r1[0] === r1[2]) win.push(r1[0]);
  if (r2[0] === r2[1] && r2[0] === r2[2]) win.push(r2[0]);
  if (r3[0] === r3[1] && r3[0] === r3[2]) win.push(r3[0]);
  if (r1[0] === r2[0] && r1[0] === r3[0]) win.push(r1[0]);
  if (r1[1] === r2[1] && r1[1] === r3[1]) win.push(r1[1]);
  if (r1[2] === r2[2] && r1[2] === r3[2]) win.push(r1[2]);
  if (r1[0] === r2[1] && r1[0] === r3[2]) win.push(r1[0]);
  if (r1[2] === r2[1] && r1[2] === r3[0]) win.push(r1[2]);
  if ((win.includes("X") && win.includes("O")) || win.length < 1) return "Tie";
  return win[0];
};

console.log(
  whoWon([
    ["O", "O", "X"],
    ["X", "X", "X"],
    ["O", "O", "O"],
  ])
);
